# Software build process

- released things should be identifiable
- builds should be reliable
- builds should be repeatable


## Build function

    SVG files + CSV control file  --ttf8-->  ranalina.ttf

Elaboration, SVG files: Some SVG files (for emdash and similar) are
generated from a script:

    gen-dash.sh -> emdash.svg + others

In this project, those generated file are committed to version
control, and not expected to change very much.
So it's entirely possible that you'll go years without needing
to run `gen-dash.sh`.

Elaboration, ttf8: In fact a few different Font 8 tools are used,
but `ttf8` is the main one.


## pre-requisites

- Unix and shell: absolutely necessary;
- Font 8 tools: absolutely necessary;
- otfinfo: useful, install with `brew install lcdf-typetools`;


## procedure

Ensure you have release of [Font 8's ttf8
tool](https://git.sr.ht/~drj/font8) on your `PATH`.

    make

This is best done in a clean clone of the repo, but
I understand that it will be done frequently when developing the font.
In that case, because the `Makefile` does not correctly specify
all the dependencies (they change, so it can't), you should use:

    touch A.svg ; make

This creates a TrueType font file `ranalina.ttf`


## Release process

All releases are made on a short stub branch from `main`.

The current date is converted to a version by taking the year as
an integer and the month and day as a 4 digit fractional part.
Make a commit if needed to ensure that
the current `main` has `Version DRAFT YYYY.MMDD` in
the NameID 5 field of the name table,
controlled by `control-ranalina.csv` file.

You can check this by compiling the TTF file with
`touch A.svg; make` and then using `otfinfo --info ranalina.ttf`
(if you have `otfinfo` installed).

Make a version branch in `git`:

    git checkout -b version/0.YYYYMMDD

with git switched to that branch
(which it will be if you just used `git checkout -b` as above),
make a new commit to change the `control-ranalina.csv` file:

- change name from Ranalint to Ranalina;
- remove DRAFT from Version name.

(remember to commit that)

    make clean
    make

Add `ranalina.ttf` as a committed file.
That's your build output.


## Versions and releases

Version marks should easily distinguish between
draft versions and formal releases.

In a TrueType font file the human readable version is
a string in the name table with NameID 5
(see [Apple TrueType Reference Manual](https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6name.html).

This is therefore used to store the font version.
The field is

    Version DRAFT YYYY.MMDD

for draft versions; and,

    Version YYYY.MMDD

for release versions.

In both cases `YYYY` is the year of release as a 4 digit decimal number,
`MM` is the month of release as a 2 digit decimal number
(0 padded, counting January as 01),
`DD` is the day of release as a 2 digit decimal number
(0 padded, counting the first day of the month as 01).
This is inspired by / modelled after ISO 8601.

In addition draft versions of the font are called «Ranalint» and
released versions are called «Ranalina».

## END

# Ranalina

Ranalina is a set of letters intended to be marked out with
Frog Tape, a decorators masking tape.

Please report issues, problems, and any other feedback on the
[gitlab issue page](https://gitlab.com/drj11/ranalina/-/issues).

The tape enforces a strict monotone discipline:
there is perforce no variation in stroke width.
What the alphabet loses in expression it gains in boldness.

It is designed to be drawn out in tape,
so strokes are economised.
Letters are not drawn with the fewest possible,
but with the fewest that are consistent with the overall coherence.
A recognisable B can be done in 5 and
a recognisable C can be done in 2—both ugly.

The basic letter grid is 4 units by 5 units.
Most letters fit into this.
Nominally the unit square is 24mm × 24mm though the actual size
will depend on the chosen font scaling.
For design purposes the unit square is divided, like a chess board,
into 8 × 8 sub-squares.

Diagonal strokes are generally designed so that two of their
corners are on a grid point (and the other two are chosen to be
at right angles).
Occasionally (most notably in X) other constraints are chosen.

Experience with actual tape suggests that using a midpoint as a
design control point is actually quite tricky, so these are no
only used where strictly necessary.
For example, V used to be tied by its midpoints, but
now is tied by its corners.

Letters like B, D, U have corners that are cut-in.
Generally the excluded part is 5/8 of a unit, making the overlap 3/8.


## Typographic programme status review:

- Latin Alphabet, uppercase: complete in ASCII and Latin-1 Supplement,
  including eth, oslash, thorn, germandbls, ae; also oe.
- Latin Alphabet, lowercase: out of scope. In the ASCII and
  Latin-1 Supplement block, lowercase letters are aliased to the
  glyphs for their uppercase forms.
- Greek, Cyrillic: out of scope (mu present in Latin-1)
- Accents: grave, acute, circumflex, tilde, macron, dot above,
  diaeresis, ring above, caron available in combining forms,
  and have anchors to combine on letters and numbers.
  The ones that have spacing forms in the Latin-1 Supplement
  block are also present.
  Cedilla is available as a spacing mark (it's rubbish) and
  as Ccedilla.
- Analphabetics: complete in the ASCII and Latin-1 Supplement block.
- Miscellania: Everything else in Latin-1 complete
  (ordinal indicators, fractions, superscripts);
  endash emdash and U+2015 are present;
  U+2219 BULLET OPERATOR present (identical to periodcentered).
- Remainder: spaces? figure dash, Mac Roman?


## Building font files

`build.md` for notes on
building the font into a font file, and
the software release process.


## a short report on the float change

On 2022-04-27 the `ttf8` tool was modified to fix what was
essentially a bug: it used to internally represent contours as
a sequence of relative integer offsets. Now it represents
contours as a sequence of (absolute) floating point
coordinates, and performs the conversion first to a fixed
integer grid, then to relative offset.

This is more accurate.

But it means that some grid positions have moved.
Often by a small amount of 1 or 2, but more commonly by 5 or 6
and sometimes up to 12.
Including widths and sidebearings.

This fixes a visible bug in `question` and some more or less
visible bugs in many other glyphs.
They have not all been inspected closely, but
some have been inspected closely, and most others skimmed.
In all cases this shows only improvements.

Seeing a grid change of 12 (still quite small, < 1% of height of E),
was a little bit surprising, but is explained by the fact that
because an integer offset representation was used,
multiple errors could accumulate around a contour.

In general more coordinates are multiples of 32
(the 3mm sub-grid used in the design).


## Design Notes

The font is designed on a mm grid,
with the standard stroke width set to 24mm.
The docem is set to 192.
This makes the height of the E equal to 5/8 em.
UPM is 2048, making 24mm = 256 Font Design Units (exact);
3mm (M/64) is 32 Units.

The HH spacing is 18mm.
Letters are generally placed symmetrically in
the centre of their advancewidth.

In general, if there is doubt,
refer to the Microsoft OpenType Character Design Standards:
https://docs.microsoft.com/en-us/typography/develop/character-design-standards/punctuation

The guillemets, « and », are normally (in a typical font) designed
to be vertically centred with respect to the x-height.
This alphabet has no lower case, so I have centred the
guillemets with respect to the cap-height.


### Typographic Extenders

`sTypoAscender` (and friends) are described in the OpenType
recommendations.
The total Baseline to Baseline distance is
(recall that TypoDescender is typically negative,
so this is the sum of three positive quantities):

sTypoAscender - TypoDescender + sTypoLineGap

That document also suggests that setting it to one em is reasonable.
I guess they never had to type ascenders or capitals
with accents on.

In Ranalina, the accent with the highest positive extension
is u030A and when placed on E it extends 2 units, 48mm,
above the cap line.
7 units from baseline.

The accent with the most negative extension is
(actually not an accent, it's an accent as a standalone glyph,
it's) u00B8, which extends to 1.75 units below the baseline.

Adding 0.125 units above that and 0.125 units below makes
the total vertical extent 9 units.
We can declare the em box arbitrarily,
let's we make the em box 8 units high (192mm).

- ascender: 57/64
- descender -15/64


### Tip for editors

The font is designed and drawn in Inkscape.
Except for a few generated glyphs (see `gen-dash.sh`).

`ttf8` ignores any transforms.
In Inkscape Path >> Simplify (bound to Ctrl-L) appears to remove them.
Although it also appears to transform straight lines into SVG c
strokes that are imperceptibly different from straight lines.
Node tool >> select all >> convert to corner fixes that
(or something like that).


### slash

A rectangle with opposite corners at

(0.0, 4.75) to (3.0, 0.25)

### V

V is designed inside a 5 x 5 grid.
It is the union of a slash and a blackslash.
The leftmost stroke should have its leftmost point at
at (0.0, 4.75).

### W

W is designed inside a 7 x 5 grid.
W is a copy of the V with its rightmost stroke truncated where
the stroke's centreline crosses x=3.5.
Then reflected.
Though this leaves the shorter strokes not tied to the grid.

An alternate glyph is possible by truncating the V as above, but
then adding a full V for the right-hand part of the W.
Creating a Times New Roman style W, with two long \ strokes, and
one short / stroke and one long / stroke.

### Z

Measuring the length of the diagonal stroke
(necessary in Inkscape, not so much when using tape):
Observe, the diagonal stroke is a rectangle;
the diagonal of that diagonal is shared with
the diagonal of a 3 x 4 rectangle.
Therefore x**2 + 1**2 = 3**2 + 4**2;
x = 24**0.5.

### cent and dollar

cent and dollar use the same diagonal stroke.

### oslash

Surprisingly, the diagonal stroke is exactly on the grid.

# END

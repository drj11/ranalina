package main

// dash

import (
	"encoding/xml"
	"flag"
	"fmt"
	// "gitlab.com/font8/x/pathdata"
	"log"
	"os"
)

// XML Models
type SVG struct {
	// The lower case "svg" is required for Inkscape.
	XMLName xml.Name `xml:"http://www.w3.org/2000/svg svg"`
	Height  string   `xml:"height,attr"`
	Width   string   `xml:"width,attr"`
	ViewBox string   `xml:"viewBox,attr"`
	Groups  []G      `xml:"g"`
}
type G struct {
	Id            string `xml:"id,attr"`
	InkscapeLabel string `xml:"http://www.inkscape.org/namespaces/inkscape label,attr"`
	InkscapeMode  string `xml:"http://www.inkscape.org/namespaces/inkscape groupmode,attr"`
	Paths         []Path `xml:"path"`
	Name          string `xml:"name,attr"`
	Groups        []G    `xml:"g"`
}
type Path struct {
	Class     string `xml:"class,attr"`
	Id        string `xml:"id,attr"`
	D         string `xml:"d,attr"`
	Style     string `xml:"style,attr"`
	Transform string `xml:"transform,attr"`
}

// A 2D point
type Point struct {
	x, y float64
}

var StrokeWidthArg = flag.Float64("strokewidth", 24.0,
	"Stroke width to use")

var StrokeHeightArg = flag.Float64("strokeheight", 60.0,
	"Height of centre of stroke")

var BaselineXArg = flag.Float64("baselinex", 120.0,
	"X coordinate (SVG) of baseline")

var BaselineYArg = flag.Float64("baseliney", 240.0,
	"Y coordinate (SVG) of baseline")

var NameArg = flag.String("name", "emdash",
	"name of glyph")

var SideBearingArg = flag.Float64("sidebearing", 0.0,
	"side bearing (same value on both left and right)")

func main() {
	flag.Parse()

	name := *NameArg

	for _, arg := range flag.Args() {
		var width float64
		fmt.Sscanf(arg, "%f", &width)
		err := Scrobble(name, *StrokeWidthArg,
			width, *SideBearingArg, *StrokeHeightArg,
			*BaselineXArg, *BaselineYArg)
		if err != nil {
			log.Print(err)
		}
	}
}

func Scrobble(name string,
	strokewidth,
	width, sidebearing, height, baselinex, baseliney float64) error {
	y := baseliney - height + strokewidth*0.5
	minx := baselinex + sidebearing
	rect := []Point{{minx, y}, {minx, y - strokewidth},
		{minx + width, y - strokewidth}, {minx + width, y}}

	svg := SVG{Height: "450mm", Width: "450mm", ViewBox: "0 0 450 450"}

	baseline := Path{D: D([]Point{{baselinex, baseliney},
		{baselinex + sidebearing + width + sidebearing, baseliney}}),
		Id:    "ttf8.baseline." + name,
		Style: "stroke:#ff0000;stroke-width:2",
	}

	outline := Path{D: D(rect)}
	group := G{Id: name,
		Paths: []Path{outline, baseline}}

	layer := G{InkscapeLabel: "Outline", InkscapeMode: "layer",
		Groups: []G{group},
	}
	svg.Groups = []G{layer}

	fmt.Println(svg)
	return WriteSVG(name+".svg", &svg)
}

func D(path []Point) string {
	s := "M"
	for _, p := range path {
		s += fmt.Sprintf(" %.6g %.6g", p.x, p.y)
	}
	return s
}

func WriteSVG(pathname string, svg *SVG) error {
	bs, err := xml.MarshalIndent(svg, "", "  ")

	w, err := os.OpenFile(pathname,
		os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer w.Close()
	w.Write(bs)

	return nil
}

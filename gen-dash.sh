#!/bin/sh

# Script to generate dashes etc

dash/dash -name emdash 120
dash/dash -name endash 60
# U+2015 HORIZONTAL BAR
dash/dash -name u2015 180
# U+2219 BULLET OPERATOR
dash/dash -name u2219 -sidebearing 9 24

#!/bin/sh

# one-off script to run tie and update all the SVG files.

set -u -e

rm -f out-*.svg

tie/tie *.svg

for f in out-*.svg
do
    mv ${f} ${f#out-}
done

# proj

Usage:

    proj a.svg b.svg ...

Short for _project_, `proj` projects each coordinate onto
the x- and y-axes separately,
producing a list of all xs and all ys.

## Letters with only axis-aligned rectangles

A B C D E F H I J L O P T U


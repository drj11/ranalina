package main

// proj

// Project onto x- and y-.

import (
	"encoding/xml"
	"flag"
	"fmt"
	"gitlab.com/font8/x/pathdata"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"io/ioutil"
	"log"
	"os"
	"sort"
)

// XML Models
type SVG struct {
	// The lower case "svg" is required for Inkscape.
	XMLName xml.Name `xml:"http://www.w3.org/2000/svg svg"`
	Height  string   `xml:"height,attr"`
	Width   string   `xml:"width,attr"`
	ViewBox string   `xml:"viewBox,attr"`
	Groups  []G      `xml:"g"`
}
type G struct {
	Id           string `xml:"id,attr"`
	InkscapeMode string `xml:"http://www.inkscape.org/namespaces/inkscape groupmode,attr"`
	Paths        []Path `xml:"path"`
	Name         string `xml:"name,attr"`
	Groups       []G    `xml:"g"`
}
type Path struct {
	Class     string `xml:"class,attr"`
	Id        string `xml:"id,attr"`
	D         string `xml:"d,attr"`
	Style     string `xml:"style,attr"`
	Transform string `xml:"transform,attr"`
}

func main() {
	flag.Parse()

	for _, arg := range flag.Args() {
		err := Process(arg)
		if err != nil {
			log.Print(err)
		}
	}
}

func Process(pathname string) error {
	r, err := os.Open(pathname)
	if err != nil {
		return err
	}

	svg, err := ReadSVG(r)
	if err != nil {
		return err
	}

	svg.Groups = CleanGroups(svg)

	for _, g := range svg.Groups {
		g := Abs(g)
		g = Reg(g)
		xs, ys := Project(g, pathname)
		PrettyPrint(pathname+" xs", xs)
		PrettyPrint(pathname+" ys", ys)
	}
	return nil
}

// Printout a sequence of floating point numbers,
// all associated with the given prefix.
func PrettyPrint(prefix string, vs []float64) {

	count := map[float64]int{}
	// unique values
	uvs := []float64{}
	// position
	pos := map[float64]int{}

	for _, f := range vs {
		if count[f] == 0 {
			uvs = append(uvs, f)
		}
		count[f] += 1
	}

	W := WinSize()

	// The last unique value printed;
	// used to join close values.
	last := -999.0

	gs := PrettyGroup(W-len([]rune(prefix)), uvs)

	for i, g := range gs {
		p := 0
		auxrow := ""

		if i == 0 {
			fmt.Print(prefix)
		} else {
			fmt.Printf("%*s", len([]rune(prefix)), "")
		}

		p += len([]rune(prefix))
		for _, u := range g {
			joiner := " "
			if u-last < 0.5 {
				joiner = "≈"
			}
			last = u
			s := fmt.Sprint(joiner, u)
			fmt.Print(s)
			pos[u] = p
			p += len([]rune(s))
			auxrow += fmt.Sprintf(" %*d",
				len([]rune(s))-1, count[u])
		}
		fmt.Print("\n")
		fmt.Printf("%*s%s\n", len([]rune(prefix)), "", auxrow)
	}
}

func PrettyGroup(width int, vs []float64) [][]float64 {
	groups := [][]float64{}

	group := []float64{}
	p := 0
	for _, v := range vs {
		s := fmt.Sprint(" ", v)
		if p+len([]rune(s)) >= width {
			groups = append(groups, group)
			group = []float64{}
			p = 0
		}
		group = append(group, v)
		p += len([]rune(s))
	}
	if len(group) > 0 {
		groups = append(groups, group)
	}
	return groups
}

func Abs(g G) G {
	paths := []Path{}
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		commands = pathdata.Absolute(commands)
		path.D = pathdata.ToString(commands)
		paths = append(paths, path)
	}
	g.Paths = paths
	return g
}

func Reg(g G) G {
	paths := []Path{}
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		commands = pathdata.Regularise(commands)
		path.D = pathdata.ToString(commands)
		paths = append(paths, path)
	}
	g.Paths = paths
	return g
}

func Project(g G, name string) (xs, ys []float64) {
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		for _, command := range commands {
			for i := 0; i < len(command.Parameter); i += 2 {
				xs = append(xs, command.Parameter[i])
				ys = append(ys, command.Parameter[i+1])
			}
		}
	}

	sort.Float64s(xs)
	sort.Float64s(ys)
	return xs, ys
}

// Read the entire input into an SVG model.
func ReadSVG(r io.Reader) (*SVG, error) {
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	svg := SVG{}
	xml.Unmarshal(bs, &svg)
	return &svg, nil
}

func CleanGroups(svg *SVG) []G {
	groups := []G{}
	var flatten func([]G)
	flatten = func(gs []G) {
		for _, g := range gs {
			// append leaf groups, but
			// ignore those that are Inkscape layers.
			if len(g.Groups) == 0 && g.InkscapeMode != "layer" {
				groups = append(groups, g)
			}
			flatten(g.Groups)
		}
	}
	flatten(svg.Groups)

	return groups
}

// Return the recommended window width
// (which is its actual width in cells if that can be determined,
// or 80 otherwise)
func WinSize() int {
	width, _, err := terminal.GetSize(0)
	if err != nil {
		return 80
	}
	return width
}

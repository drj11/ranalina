package main

// rectangularity

// Compute the rectangularity of the polygons in SVG files.

import (
	"encoding/xml"
	"flag"
	"fmt"
	"gitlab.com/font8/x/pathdata"
	"io"
	"io/ioutil"
	"log"
	"math"
	"os"
)

var xmlp = flag.Bool("xml", false, "output xml")
var scorep = flag.Float64("score", 0.001, "hide scores better than this")

// XML Models
type SVG struct {
	// The lower case "svg" is required for Inkscape.
	XMLName xml.Name `xml:"http://www.w3.org/2000/svg svg"`
	Height  string   `xml:"height,attr"`
	Width   string   `xml:"width,attr"`
	ViewBox string   `xml:"viewBox,attr"`
	Groups  []G      `xml:"g"`
}
type G struct {
	Id           string `xml:"id,attr"`
	InkscapeMode string `xml:"http://www.inkscape.org/namespaces/inkscape groupmode,attr"`
	Paths        []Path `xml:"path"`
	Name         string `xml:"name,attr"`
	Groups       []G    `xml:"g"`
}
type Path struct {
	Class string `xml:"class,attr"`
	Id    string `xml:"id,attr"`
	D     string `xml:"d,attr"`
	Style string `xml:"style,attr"`
	// Transform string `xml:"transform,attr"`
}

func main() {
	flag.Parse()

	for _, arg := range flag.Args() {
		err := Process(arg)
		if err != nil {
			log.Print(err)
		}
	}
}

func Process(pathname string) error {
	r, err := os.Open(pathname)
	if err != nil {
		return err
	}

	svg, err := ReadSVG(r)
	if err != nil {
		return err
	}

	svg.Groups = CleanGroups(svg)
	gs := []G{}
	for _, g := range svg.Groups {
		g := Abs(g)
		g = Reg(g)
		Rectangularity(g, pathname)
		gs = append(gs, g)
	}
	svg.Groups = gs

	bs, err := xml.MarshalIndent(svg, "", "  ")
	if *xmlp {
		os.Stdout.Write(bs)
	}
	return err
}

func Abs(g G) G {
	paths := []Path{}
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		commands = pathdata.Absolute(commands)
		path.D = pathdata.ToString(commands)
		paths = append(paths, path)
	}
	g.Paths = paths
	return g
}

func Reg(g G) G {
	paths := []Path{}
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		commands = pathdata.Regularise(commands)
		path.D = pathdata.ToString(commands)
		paths = append(paths, path)
	}
	g.Paths = paths
	return g
}

func Rectangularity(g G, name string) {
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		score := Rect(commands[0].Parameter)
		if score > 90 {
			continue
		}
		if score < *scorep {
			continue
		}
		fmt.Printf("%s %.3g %v\n", name, score, commands)
	}
}

func Rect(ps []float64) float64 {
	if len(ps) != 8 {
		return 99
	}
	// Consider quadrilaterial,
	// A   B
	// D   C
	// dot product of B-A and B-C
	a := ndot(ps[2]-ps[0], ps[3]-ps[1], ps[2]-ps[4], ps[3]-ps[5])
	// dot product of D-A and D-C
	b := ndot(ps[6]-ps[0], ps[7]-ps[1], ps[6]-ps[4], ps[7]-ps[5])
	// difference of distance between AC and BC
	d1 := hypot(ps[0]-ps[4], ps[1]-ps[5])
	d2 := hypot(ps[2]-ps[6], ps[3]-ps[7])
	d := (d1 - d2) / ((d1 + d2) / 2)
	return abs(a) + abs(b) + abs(d)
}

func abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func ndot(x1, y1, x2, y2 float64) float64 {
	return (x1*x2 + y1*y2) / (hypot(x1, y1) * hypot(x2, y2))
}

func hypot(x, y float64) float64 {
	return math.Sqrt(x*x + y*y)
}

// Read the entire input into an SVG model.
func ReadSVG(r io.Reader) (*SVG, error) {
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	svg := SVG{}
	xml.Unmarshal(bs, &svg)
	return &svg, nil
}

func CleanGroups(svg *SVG) []G {
	groups := []G{}
	var flatten func([]G)
	flatten = func(gs []G) {
		for _, g := range gs {
			// append leaf groups, but
			// ignore those that are Inkscape layers.
			if len(g.Groups) == 0 && g.InkscapeMode != "layer" {
				groups = append(groups, g)
			}
			flatten(g.Groups)
		}
	}
	flatten(svg.Groups)

	return groups
}

# tie

Usage:

    tie [-grid 0,6,9,15,18,24] a.svg b.svg ...

The default grid is 0,5,10,12.5,15,20,25.

Show how closely polygon corners are to a grid.
The current report computes distances to the nearest grid point,
and shows median (Q₂), maximum (Q₄), and median absolute deviation
(MAD, a measure of dispersal).

Following the distance statistics is a "tie count" for each
polygon in the glyphs groups.
The number of nodes that are not on a grid position is reported
(so 0 means all nodes aligned to grid; 4 means none are).
A hardwired threshold is used.

To produce a report sorted by the maximum deviation from the grid:

    tie/tie *.svg | sort -k 5g

## Letters with only axis-aligned rectangles

A B C D E F H I J L O P T U

## Transform

When considering transforming from one grid to another.
Based on node tie count:

0:  transform on grid; check rectangularity

1:  ?

2:  if adjacent?
    if opposite, transform tied nodes on grid,
    remaining nodes should be picked so that the sides that were
    original 25mm are now 24mm and preserving rectangularity

3:  transform tied point, opposite point, and fix other 2 as per
    case 2.

export LANG =

ranalina.ttf: A.svg Z.svg emdash.svg control-ranalina.csv
	ttf8 -dir ranalina -control control-ranalina.csv *.svg
	f8name -dir ranalina control-ranalina.csv
	f8dsig -dir ranalina
	assfont -dir ranalina

clean:
	rm *.ttf || true
	rm *.ttx || true
	rm ranalina/* || true

emdash.svg: gen-dash.sh
	sh gen-dash.sh

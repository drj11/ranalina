package main

// tie

import (
	"encoding/xml"
	"flag"
	"fmt"
	"gitlab.com/font8/x/pathdata"
	"io"
	"io/ioutil"
	"log"
	"math"
	"os"
	"sort"
	"strings"
)

// XML Models
type SVG struct {
	// The lower case "svg" is required for Inkscape.
	XMLName   xml.Name  `xml:"http://www.w3.org/2000/svg svg"`
	Height    string    `xml:"height,attr"`
	Width     string    `xml:"width,attr"`
	ViewBox   string    `xml:"viewBox,attr"`
	Namedview Namedview `xml:"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd namedview"`
	Groups    []G       `xml:"g"`
}
type Namedview struct {
	DocumentUnits string       `xml:"http://www.inkscape.org/namespaces/inkscape document-units,attr"`
	Showgrid      bool         `xml:"showgrid,attr"`
	InkscapeGrid  InkscapeGrid `xml:"http://www.inkscape.org/namespaces/inkscape grid"`
}
type InkscapeGrid struct {
	Type       string `xml:"type,attr"`
	Units      string `xml:"units,attr"`
	Spacingx   string `xml:"spacingx,attr"`
	Spacingy   string `xml:"spacingy,attr"`
	Empspacing string `xml:"empspacing,attr"`
}
type G struct {
	Id            string `xml:"id,attr"`
	InkscapeLabel string `xml:"http://www.inkscape.org/namespaces/inkscape label,attr"`
	InkscapeMode  string `xml:"http://www.inkscape.org/namespaces/inkscape groupmode,attr"`
	Paths         []Path `xml:"path"`
	Name          string `xml:"name,attr"`
	Groups        []G    `xml:"g"`
}
type Path struct {
	Class     string `xml:"class,attr"`
	Id        string `xml:"id,attr"`
	D         string `xml:"d,attr"`
	Style     string `xml:"style,attr"`
	Transform string `xml:"transform,attr"`
}

// Create a model for an inkscape:grid that has lines
// every min mm and major lines every mod.
func MakeInkscapeGrid(min float64, mod int) InkscapeGrid {
	grid := InkscapeGrid{Type: "xygrid", Units: "mm",
		Spacingx: fmt.Sprint(min), Spacingy: fmt.Sprint(min),
		Empspacing: fmt.Sprint(mod)}
	return grid
}

// A 1D grid.
type Grid struct {
	V []float64 // The points in the grid
	M float64   // The grid modulus
}

// A 2D point, plus some extra data.
type Point struct {
	x, y     float64
	tie      bool // tied to grid?
	distance float64
}

// The default grid for the 25mm version is 25 divided into 5
// spaces, with the addition of the midpoint.
var GridArg = flag.String("grid",
	"0,5,10,12.5,15,20,25", "grid coordinates")

var RegridArg = flag.String("regrid",
	"0,6,9,12,15,18,24", "regrid coordinates")

var InkscapeGridArg = flag.String("inkscapegrid",
	"3,8", "Inkscape grid to use in output")

var Threshold = 0.01

func main() {
	flag.Parse()

	grid := ParseGrid(*GridArg)
	regrid := ParseGrid(*RegridArg)
	inkscape := ParseGrid(*InkscapeGridArg)
	inkscapegrid := MakeInkscapeGrid(inkscape.V[0], int(inkscape.M))

	for _, arg := range flag.Args() {
		err := Process(grid, regrid, inkscapegrid, arg)
		if err != nil {
			log.Print(err)
		}
	}
}

// Parse the grid argument into a list.
func ParseGrid(list string) Grid {
	ss := strings.Split(list, ",")
	fs := []float64{}

	for _, s := range ss {
		var f float64
		fmt.Sscanf(s, "%f", &f)
		fs = append(fs, f)
	}

	return Grid{fs[:len(fs)-1], fs[len(fs)-1]}
}

// Process a single file.
func Process(grid, regrid Grid, inkscapegrid InkscapeGrid, pathname string) error {
	r, err := os.Open(pathname)
	if err != nil {
		return err
	}

	svg, err := ReadSVG(r)
	if err != nil {
		return err
	}

	svg.Groups = CleanGroups(svg)

	replacement_groups := GridGroups(
		grid, regrid, pathname, svg.Groups)

	nsvg := *svg
	layer := G{InkscapeLabel: "Outline", InkscapeMode: "layer",
		Groups: replacement_groups,
	}
	nsvg.Groups = []G{layer}
	nsvg.Namedview.InkscapeGrid = inkscapegrid

	return WriteSVG("out-"+pathname, &nsvg)
}

func GridGroups(grid, regrid Grid, pathname string, groups []G) []G {
	// replacements groups
	rgs := []G{}

	for _, g := range groups {
		g := Abs(g)
		g = Reg(g)
		ng := g

		all_ds := []float64{}
		// list of (tied,length) pairs for each path.
		counts := [][]int{}

		fmt.Print(pathname, "/", g.Id, " counts")

		ls := GridTie(grid, g, pathname)
		// one list per path
		for i, ps := range ls {
			count := 0
			for _, p := range ps {
				if p.tie {
					count++
				}
				all_ds = append(all_ds, p.distance)
			}

			counts = append(counts, []int{count, len(ps)})
			fmt.Print(" ", count, "/", len(ps))

			ps = Regrid(grid, regrid, pathname, ps)

			// PrintPoints(ps)
			ng.Paths[i].D = D(ps)
		}

		fmt.Println()

		summary := Summarise(all_ds)
		fmt.Printf("%s/%s Q₂ %.6g Q₄ %.6g MAD %.6g\n",
			pathname, g.Id, summary[0], summary[1], summary[2])

		rgs = append(rgs, ng)
	}
	return rgs
}

// Regrid a set of points by moving from one grid to another.
func Regrid(grid, regrid Grid, pathname string, ps []Point) []Point {
	count := 0
	for _, p := range ps {
		if p.tie {
			count++
		}
	}

	regridder := MakeRegridder(grid, regrid)

	switch {
	case count == len(ps), count == 0:
		for i, p := range ps {
			ps[i] = regridder(p)
		}
	case 4 == len(ps) && (2 == count || 1 == count):
		ps = Regrid2(regridder, grid.M, regrid.M, ps)
	default:
		log.Fatal("unhandled regrid case ", count, len(ps))
	}
	return ps
}

// Regrid a rectangle that has 2 tied corners.
// (And also 1 tied corner, by tying the opposite corner)
// M and N are the moduluses of grid and regrid.
func Regrid2(regridder func(Point) Point, M, N float64, ps []Point) []Point {
	// Rotate representation of the rectangle,
	// until ps[0] is a tied point and ps[3] is not.
	// Corollary: exactly one of ps[1] and ps[2] is tied.
	// By de Morgan's transformation:
	// !(ps[0] && !ps[3]) <=> !ps[0] || ps[3]
	for !ps[0].tie || ps[3].tie {
		ps = append(ps[1:], ps[0])
	}

	if ps[1].tie {
		// adjacent corners tied
		ps = FixAdjacent(regridder, N, ps)
	} else {
		// opposite corners
		if abs(Distance(ps[1], ps[0])-M) < Threshold {
			ps = FixBetween(regridder, M, N, ps[0], ps[1], ps[2])
		} else if abs(Distance(ps[1], ps[2])-M) < Threshold {
			ps = FixBetween(regridder, M, N, ps[2], ps[1], ps[0])
		} else {
			log.Fatal("Couldn't find edge of length ", M, ps)
		}
	}

	return ps
}

// Fix the points ps[2] and ps[3] that are adjacent to tied
// points ps[1] and ps[0] respectively.
func FixAdjacent(regridder func(Point) Point, N float64, ps []Point) []Point {
	// Vector from ps[0] to ps[3]
	v03 := ps[3].Sub(ps[0])
	// Vector perpendicular to ps[0] ps[1]
	perp01 := ps[1].Sub(ps[0]).Perp()

	// v03 . perp01, positive is in same direction
	product := v03.x*perp01.x + v03.y*perp01.y
	// ps[3] is on left of ray from ps[0] to ps[1]
	left := product > 0

	ns := make([]Point, 4)
	ns[0] = regridder(ps[0])
	ns[1] = regridder(ps[1])

	// recompute perp01 for regridded vectors
	perp01 = ns[1].Sub(ns[0]).Perp()
	if !left {
		perp01 = perp01.Scale(-1)
	}

	// unit vector perpendicular to 01
	perp_unit := perp01.Scale(1.0 / Distance(ns[1], ns[0]))
	v03prime := perp_unit.Scale(N)
	ns[3] = ns[0].Add(v03prime)
	ns[2] = ns[1].Add(v03prime)
	// Ensure clockwise orientation
	if left {
		ns[1], ns[3] = ns[3], ns[1]
	}
	return ns
}

// Fix the point b (between a and c) so that a', b', c' is a right angle;
// b' will be at distance N from a', and on the same side of the
// line a' c' as b is to ac.
// where a' and c' are a and c after regridding.
func FixBetween(regridder func(Point) Point, M, N float64, a, b, c Point) []Point {
	// Vector from A to C
	ac := c.Sub(a)
	// Vector perpendicular to ac.
	perp_ac := ac.Perp()
	// Vector from A to B
	ab := b.Sub(a)

	// ab . perp(ac), positive if in same direction
	product := ab.x*perp_ac.x + ab.y*perp_ac.y
	// b is on the left of ray from a to c.
	left := product > 0

	// Length a' b' is N; a' b' c' is right-angle.
	// Lengths a' b', b' c', a' c' are related by Pythagoras' Theorem.

	a_prime := regridder(a)
	c_prime := regridder(c)
	a_c_prime := c_prime.Sub(a_prime)

	len_a_c_prime := Distance(a_prime, c_prime)

	// Basic trigonometry.
	sin_c_prime := N / len_a_c_prime
	cos_a_prime := sin_c_prime
	sin_a_prime := math.Sqrt(1 - math.Pow(cos_a_prime, 2))
	if !left {
		sin_a_prime *= -1
	}

	ac_unit := Point{x: a_c_prime.x / len_a_c_prime,
		y: a_c_prime.y / len_a_c_prime}
	ac_unit_perp := Point{x: -ac_unit.y, y: ac_unit.x}

	// first make a unit vector in direction a' b'.
	a_b_prime := ac_unit.Scale(cos_a_prime).Add(ac_unit_perp.Scale(sin_a_prime))
	// Scale to length N
	a_b_prime = a_b_prime.Scale(N)

	// Clockwise orientation iff b is on left.
	rs := []Point{a_prime, a_prime.Add(a_b_prime), c_prime, c_prime.Sub(a_b_prime)}
	// flip orientation if b is on right.
	if !left {
		rs[1], rs[3] = rs[3], rs[1]
	}
	return rs
}

func (a Point) Sub(b Point) Point {
	return Point{x: a.x - b.x, y: a.y - b.y}
}

func (a Point) Add(b Point) Point {
	return Point{x: a.x + b.x, y: a.y + b.y}
}

// Vector perpendicular to a (a rotated 90 degree anti-clocwise)
func (a Point) Perp() Point {
	return Point{x: -a.y, y: a.x}
}

func (a Point) Scale(s float64) Point {
	return Point{x: a.x * s, y: a.y * s}
}

func MakeRegridder(grid, regrid Grid) func(Point) Point {
	grid.V = append([]float64{grid.V[len(grid.V)-1] - grid.M}, grid.V...)
	grid.V = append(grid.V, grid.V[1]+grid.M)

	regrid.V = append([]float64{regrid.V[len(regrid.V)-1] - regrid.M}, regrid.V...)
	regrid.V = append(regrid.V, regrid.V[1]+regrid.M)

	regridder := func(x float64) float64 {
		n := math.Floor(x / grid.M)
		x -= n * grid.M

		i := sort.SearchFloat64s(grid.V, x)
		if grid.V[i] != x {
			i--
		}
		if grid.V[i] > x {
			log.Fatal("mysteriously, x < grid.V[i] ", x, grid)
		}
		if x >= grid.V[i+1] {
			log.Fatal("mysteriously, x >= grid.V[i+1] ", x, grid)
		}

		y := Lerp(grid.V[i], grid.V[i+1], regrid.V[i], regrid.V[i+1], x)
		return n*regrid.M + y
	}
	regrid_point := func(p Point) Point {
		return Point{x: regridder(p.x), y: regridder(p.y)}
	}
	return regrid_point
}

// Interpolate between t and u
// to the same extent that x is between p and q
func Lerp(p, q, t, u, x float64) float64 {
	l := (x - p) / (q - p)
	return t + l*(u-t)
}

func D(path []Point) string {
	s := "M"
	for _, p := range path {
		s += fmt.Sprintf(" %.6g %.6g", p.x, p.y)
	}
	return s
}

func PrintPoints(ps []Point) {
	if len(ps) <= 0 {
		fmt.Println("EMPTY")
		return
	}

	// Extended edition, with first point appended.
	xs := append([]Point(nil), ps...)
	xs = append(xs, ps[0])

	fmt.Printf("POLY[%d]\n", len(ps))
	for i := range ps {
		p := xs[i]
		q := xs[i+1]
		s := "-"
		if ps[i].tie {
			s = "T"
		}
		d := math.Hypot(q.x-p.x, q.y-p.y)
		fmt.Printf("  %.6g %.6g %s %.6g\n", p.x, p.y, s, d)
	}
}

// Summarise a list of numbers.
func Summarise(xs []float64) []float64 {
	N := len(xs)

	median := Median(xs)
	max := xs[N-1]

	// (absolute) deviations
	deviations := make([]float64, N)
	for i, x := range xs {
		deviations[i] = abs(x - median)
	}

	mad := Median(deviations)
	return []float64{median, max, mad}
}

func Median(xs []float64) float64 {
	sort.Float64s(xs)
	N := len(xs)
	h := (N - 1) / 2
	i := N / 2
	return (xs[h] + xs[i]) / 2
}

// Convert a path to absolute coordinates.
// Currently introduces a very small distortion
// (of the order of 1 or a few ulp for float64).
func Abs(g G) G {
	paths := []Path{}
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		commands = pathdata.Absolute(commands)
		path.D = pathdata.ToString(commands)
		paths = append(paths, path)
	}
	g.Paths = paths
	return g
}

// Regularise. Whatever that is.
func Reg(g G) G {
	paths := []Path{}
	for _, path := range g.Paths {
		commands := pathdata.PathDataCommands(path.D)
		commands = pathdata.Regularise(commands)
		path.D = pathdata.ToString(commands)
		paths = append(paths, path)
	}
	g.Paths = paths
	return g
}

// Returns a list of list of distances;
// one list for each path in the group.
func GridTie(grid Grid, g G, name string) [][]Point {
	if len(grid.V) == 0 {
		log.Fatal("grid should not be empty")
	}

	ls := [][]Point{}

	for _, path := range g.Paths {
		ties := []Point{}

		commands := pathdata.PathDataCommands(path.D)
		for _, command := range commands {
			for i := 0; i < len(command.Parameter); i += 2 {
				x := command.Parameter[i]
				y := command.Parameter[i+1]

				dx, nx := Nearest(grid, x)
				dy, ny := Nearest(grid, y)

				distance := math.Hypot(dx, dy)

				tie := false
				if distance < Threshold {
					tie = true
					x = nx
					y = ny
				}
				ties = append(ties, Point{x, y, tie, distance})
			}
		}
		ls = append(ls, ties)
	}

	return ls
}

// Find the nearest (1D) grid point to x.
// the last entry in the list is the grid repeat (the modulus);
// the earlier entries are the grid points
// (note that the last entry, the modulus, is not of itself a grid point).
func Nearest(grid Grid, x float64) (distance, point float64) {
	// Modify the grid by adding sentinel points at both ends.
	gs := append([]float64(nil), grid.V[:len(grid.V)]...)
	first := gs[0]
	last := gs[len(gs)-1]
	gs = append([]float64{last - grid.M}, gs...)
	gs = append(gs, first+grid.M)

	floor := grid.M * math.Floor(x/grid.M)
	x -= floor

	d := 999.0
	v := 999.0

	for _, g := range gs {
		if abs(x-g) < d {
			d = abs(x - g)
			v = g
		}
	}
	return d, floor + v
}

func abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func Distance(p, q Point) float64 {
	return math.Hypot(p.x-q.x, p.y-q.y)
}

// Read the entire input into an SVG model.
func ReadSVG(r io.Reader) (*SVG, error) {
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	svg := SVG{}
	xml.Unmarshal(bs, &svg)
	return &svg, nil
}

func WriteSVG(pathname string, svg *SVG) error {
	bs, err := xml.MarshalIndent(svg, "", "  ")

	w, err := os.OpenFile(pathname,
		os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer w.Close()
	w.Write(bs)

	return nil
}

func CleanGroups(svg *SVG) []G {
	groups := []G{}
	var flatten func([]G)
	flatten = func(gs []G) {
		for _, g := range gs {
			// append leaf groups, but
			// ignore those that are Inkscape layers.
			if len(g.Groups) == 0 && g.InkscapeMode != "layer" {
				groups = append(groups, g)
			}
			flatten(g.Groups)
		}
	}
	flatten(svg.Groups)

	return groups
}

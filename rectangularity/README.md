# Rectangularity

The `rectangularity` tool computes and displays the
_rectangularity_ of polygons in the SVG file.
It is only intended to be used in SVG files used in the
preparation of the `ranalina` font, so may not be general
enough, and may fail with other SVG files.

Rectangularity is a thing I have made up for this tool:
it is a metric that measures closeness to a
rectangular form.

Rectangularity of a polygon that does not have 4 vertices is 99.

Rectangularity of a polygon ABCD is
the sum of the magnitudes of 3 quantities:
- the cosine of the angle ABC (the angle at B formed by rays
  intersection A and C) (magnitude between 0 and 1);
- the cosine of the angle CDA;
- the difference in the lengths of the two diagonals divided by
  the mean of those lengths (magnitude beteen 0 and 2).

The rectangularity of a rectangle is therefore 0.

# Practical values

values < 1e-6 are probably rectangles represented as closely as
inkscape allows.

values < 1e-3 are very nearly rectangles and about as close as
you can get by judging it by eye.

values < 0.01 still look pretty rectangular.

values > 0.1 are clearly not supposed to be rectangles
(eg 2 right angles).
